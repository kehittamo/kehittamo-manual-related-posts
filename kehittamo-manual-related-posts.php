<?php
/*
Plugin Name: Kehittämö Manual Related Posts
Plugin URI: http://www.kehittamo.fi
Description: Set related posts manually but easily with great ergonomics! Stop displaying auto/random related posts!
Version: 1.8.2
Author: Kehittämö Oy (originally by Julio Potier)
Author URI: http://www.boiteaweb.fr
*/

define( 'KEHITTAMOMRP__FILE__', __FILE__ );
define( 'KEHITTAMOMRP_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'KEHITTAMOMRP_VERSION', 'Manual Related Posts' );
define( 'KEHITTAMOMRP_VERSION', '1.8.2' );

add_action( 'plugins_loaded', 'kehittamo_manual_related_posts_bootstrap' );
function kehittamo_manual_related_posts_bootstrap() {
	$folder = 'inc/';
	$where = is_admin() ? 'backend-' : 'frontend-';
	$pre_ajax = defined( 'DOING_AJAX' ) && DOING_AJAX ? '' : 'no';
	$ajax = 'ajax.inc.php';
	$filename = $folder . $where . $pre_ajax . $ajax;
	if( file_exists( plugin_dir_path( __FILE__ ) . $filename ) ) {
		include( plugin_dir_path( __FILE__ ) . $filename );
	}
	$where = 'bothend-';
	$filename = $folder . $where . $pre_ajax . $ajax;
	if( file_exists( plugin_dir_path( __FILE__ ) . $filename ) ) {
		include( plugin_dir_path( __FILE__ ) . $filename );
	}
}
