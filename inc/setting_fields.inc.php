<?php
if( !defined( 'ABSPATH' ) )
	die( 'Cheatin\' uh?' );

function kehittamomrp_field_in_content()
{
	$kehittamomrp_options = get_option( 'kehittamomrp' );
?>
	<label><input type="checkbox" name="kehittamomrp[in_content]" <?php checked( $kehittamomrp_options['in_content'], 'on' ); ?> /> <em><?php _e( 'Will be displayed at bottom of content.', 'kehittamomrp' ); ?></em></label>
	<?php if( current_user_can( 'edit_themes' ) ) { ?>
		<br />
		<em><?php _e( 'You can also use <code>&lt;?php echo do_shortcode( \'[manual_related_posts]\' ); ?&gt</code> to pull the list out.', 'kehittamomrp' ); ?></em>
	<?php } ?>
<?php
}

function kehittamomrp_so_ironic()
{ ?>
	<em><?php _e( 'So ironic ... auto related posts in manual related posts ;)', 'kehittamomrp' ); ?></em>
<?php
}

function kehittamomrp_field_post_types()
{
	$kehittamomrp_options = get_option( 'kehittamomrp' );
	global $sitepress;
	if ( isset( $sitepress ) && defined( 'ICL_LANGUAGE_CODE' ) ) {
		$lang = kehittamomrp_wpml_lang_by_code( ICL_LANGUAGE_CODE );
		$code = ICL_LANGUAGE_CODE;
		$flag = sprintf( ' <img src="%s" alt="%s" />', $sitepress->get_flag_url( $code ), $code );
	} else {
		$lang = $code = get_locale();
		$flag = '';
	}
	$kehittamomrp_options['post_types'] = !empty( $kehittamomrp_options['post_types'] ) ? $kehittamomrp_options['post_types'] : array();
	foreach ( get_post_types( array( 'public'=>true, 'show_ui'=>true ), 'objects' ) as $cpt ) {
		echo '<label><input type="checkbox" '.checked( in_array( $cpt->name, $kehittamomrp_options['post_types'] ) ? 'on' : '', 'on', false ).' name="kehittamomrp[post_types][]" value="'.esc_attr( $cpt->name ).'" /> '.esc_html( $cpt->label ).'</label><br />' .
			'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . sprintf( __( 'Front-end title for %s%s', 'kehittamomrp' ), strtolower( esc_html( $cpt->label ) ), $flag ) . '<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		if ( isset( $sitepress ) ) {
			$langs = $sitepress->get_languages( "en' AND active='1" );
		} else {
			$langs = array( array( 'default_locale'=>get_locale() ) );
		}
		foreach ( $langs as $_lang ) {
			if ( isset( $kehittamomrp_options['head_titles'][ $cpt->name ][ $_lang['default_locale'] ] ) ) {
				$value = esc_attr( $kehittamomrp_options['head_titles'][ $cpt->name ][ $_lang['default_locale'] ] );
			}elseif ( isset( $kehittamomrp_options['head_titles'][ $cpt->name ] ) && is_string( $kehittamomrp_options['head_titles'][ $cpt->name ] ) ) {
				$value = esc_attr( $kehittamomrp_options['head_titles'][ $cpt->name ] );
			} else {
				$value = __( 'You may also like:', 'kehittamomrp' );
			}
			$type = $_lang['default_locale'] != $lang ? 'hidden' : 'text';
			echo '<input type="' . $type . '" class="regular-text" name="kehittamomrp[head_titles][' . esc_attr( $cpt->name ) . '][' . $_lang['default_locale'] . ']" value="' . $value . '" />';
		}
		echo '<br />';
	}
}

function kehittamomrp_field_auto_posts()
{
	$kehittamomrp_options = get_option( 'kehittamomrp' );
?>
	<label><input type="checkbox" name="kehittamomrp[auto_posts]" value="both" <?php checked( isset( $kehittamomrp_options['auto_posts'] ) && $kehittamomrp_options['auto_posts'] == 'both', true ); ?> /> <em><?php _e( 'Use categories to find related posts.', 'kehittamomrp' ); ?></em></label>
<?php
}

function kehittamomrp_field_max_posts()
{
	$kehittamomrp_options = get_option( 'kehittamomrp' );
?>
	<label><input type="number" name="kehittamomrp[max_posts]" maxlength="3" size="2" min="0" class="small-text" value="<?php echo (int) $kehittamomrp_options['max_posts']; ?>" /> <em><?php _e( 'Including manual and auto related posts. (0 = No limit)', 'kehittamomrp' ); ?></em></label>
<?php
}

function kehittamomrp_field_random_posts()
{
	$kehittamomrp_options = get_option( 'kehittamomrp' );
?>
	<label><input type="checkbox" name="kehittamomrp[random_order]" <?php checked( isset( $kehittamomrp_options['random_order'] ) && $kehittamomrp_options['random_order'] == 'on', true ); ?> /> <em><?php _e( 'You can randomize the posts request.', 'kehittamomrp' ); ?></em></label><br />
	<label><input type="checkbox" name="kehittamomrp[random_posts]" <?php checked( isset( $kehittamomrp_options['random_posts'] ) && $kehittamomrp_options['random_posts'] == 'on', true ); ?> /> <em><?php _e( 'You can randomize the display for posts order.', 'kehittamomrp' ); ?></em></label><br />
<?php
}

function kehittamomrp_field_display_content()
{
	$kehittamomrp_options = get_option( 'kehittamomrp' );
?>
	<label><input type="checkbox" name="kehittamomrp[display_content]" <?php checked( isset( $kehittamomrp_options['display_content'] ) && $kehittamomrp_options['display_content'] != 'none', true ); ?> value="excerpt" /> <em><?php _e( 'Print the post excerpt too.', 'kehittamomrp' ); ?></em></label>
<?php
}

function kehittamomrp_field_in_content_mode()
{
	$kehittamomrp_options = get_option( 'kehittamomrp' );
	$kehittamomrp_options['in_content_mode'] = $kehittamomrp_options['in_content_mode'] ? $kehittamomrp_options['in_content_mode'] : 'list';
?>
	<label><input type="radio" name="kehittamomrp[in_content_mode]" <?php checked( $kehittamomrp_options['in_content_mode'], 'list' ); ?> value="list" /> <em><?php _e( 'List mode.', 'kehittamomrp' ); ?></em></label>
	<br />
	<label><input type="radio" name="kehittamomrp[in_content_mode]" <?php checked( $kehittamomrp_options['in_content_mode'], 'thumb' ); ?> value="thumb" /> <em><?php _e( 'Thumb mode.', 'kehittamomrp' ); ?></em></label>
<?php
}