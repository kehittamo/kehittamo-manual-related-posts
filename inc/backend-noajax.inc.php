<?php
defined( 'ABSPATH' ) ||	die( 'Cheatin\' uh?' );

function kehittamomrp_find_posts_div()
{
	$kehittamomrp_options = get_option( 'kehittamomrp' );
?>
	<div id="find-posts" class="find-box" style="display:none;">
		<div id="find-posts-head" class="find-box-head"><?php _e( 'Find related posts', 'kehittamomrp' ); ?></div>
		<div class="find-box-inside">
			<div class="find-box-search">

				<input type="hidden" name="affected" id="affected" value="" />
				<?php wp_nonce_field( 'find-posts', '_ajax_nonce', false ); ?>
				<label class="screen-reader-text" for="find-posts-input"><?php _e( 'Search' ); ?></label>
				<input type="text" id="find-posts-input" name="ps" value="" />
				<input type="button" id="find-posts-search" value="<?php esc_attr_e( 'Search' ); ?>" class="button" /> <span class="spinner"></span>
				<p style="clear:both">
				<?php
				$post_types = get_post_types( array( 'public' => true, 'show_ui' => true ), 'objects' );
				foreach( $post_types as $post ) {
					if ( 'attachment' == $post->name || ! in_array( $post->name, $kehittamomrp_options['post_types'] ) )
						continue;
					?>
					<label style="padding-left:5px" for="find-posts-<?php echo esc_attr( $post->name ); ?>"><input type="checkbox" name="find-posts-what[]" id="find-posts-<?php echo esc_attr( $post->name ); ?>" value="<?php echo esc_attr( $post->name ); ?>" checked="checked" />
						<?php echo $post->label; ?>
					</label>
					<?php
				}
				?>
				</p>
			</div>
			<div id="find-posts-response"></div>
		</div>
		<div class="find-box-buttons">
			<input id="find-posts-close" type="button" class="button" value="<?php esc_attr_e( 'Close' ); ?>" style="float:left;position:relative;width:auto;height:auto"/>
			<?php submit_button( __( 'Select' ), 'button-primary alignright', 'find-posts-submit', false ); ?>
		</div>
	</div>
<?php
}

add_action( 'add_meta_boxes','kehittamomrp_add_meta_box' );
function kehittamomrp_add_meta_box()
{
	$kehittamomrp_options = get_option( 'kehittamomrp' );
	if ( ! empty( $kehittamomrp_options['post_types'] ) ) {
		foreach( $kehittamomrp_options['post_types'] as $cpt ) {
			add_meta_box( 'kehittamomrp', KEHITTAMOMRP_VERSION, 'kehittamomrp_box', $cpt, 'side' );
		}
	}
}

function kehittamomrp_box( $post )
{
	$related_post_ids = kehittamomrp_get_related_posts( $post->ID, false );
	$related_post_ids = is_array( $related_post_ids ) ? '' : $related_post_ids;
	?>
	<div>
		<input class="hide-if-js" type="text" name="kehittamomrp_post_ids" id="kehittamomrp_post_ids" value="<?php echo esc_attr( $related_post_ids ); ?>" />&nbsp;&nbsp;
		<?php wp_nonce_field( 'add-relastedpostsids_' . $post->ID, '_wpnonce_yyarpp' ); ?>
		<div>
			<a href="javascript:void(0);" id="kehittamomrp_open_find_posts_button" class="button button-small hide-if-no-js"><?php _e( 'Add a related post', 'kehittamomrp' ); ?></a>
			<span class="hide-if-js"><?php _e( 'Add posts IDs from posts you want to relate, comma separated.', 'kehittamomrp' ); ?></span>
		</div>
		<ul id="ul_yyarpp" class="tagchecklist">
			<?php
			if ( ! empty( $related_post_ids ) ) {
				$related_post_ids = wp_parse_id_list( $related_post_ids );
				foreach( $related_post_ids as $id ) { ?>
					<li data-id="<?php echo (int)$id; ?>"><span style="float:none;"><a class="hide-if-no-js erase_yyarpp">X</a>&nbsp;&nbsp;<?php echo get_the_title( (int) $id ); ?></span></li>
				<?php
				}
			}
			?>
		</ul>
		<span class="plugins widefat"><a href="javascript:void(0);" id="kehittamomrp_delete_related_posts" class="delete hide-if-no-js"><?php _e( 'Clear List' ); ?></a></span>
	</div>
	<?php
}

add_action( 'admin_head-post.php', 'kehittamomrp_admin_head_scripts' );
add_action( 'admin_head-post-new.php', 'kehittamomrp_admin_head_scripts' );
function kehittamomrp_admin_head_scripts()
{
	$suffix = defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ? '' : '.min';
	$file = "kehittamomrp$suffix.js";
	wp_enqueue_script( 'kehittamomrp_js', plugins_url( 'js/' . $file, KEHITTAMOMRP__FILE__ ), 'jquery', KEHITTAMOMRP_VERSION, true );
	wp_localize_script( 'kehittamomrp_js', 'kehittamomrp_js', array( 'ID' => $GLOBALS['post']->ID ) );
}

add_action( 'admin_footer-post.php', 'kehittamomrp_admin_footer_scripts' );
add_action( 'admin_footer-post-new.php', 'kehittamomrp_admin_footer_scripts' );
function kehittamomrp_admin_footer_scripts()
{
	global $typenow;
	$kehittamomrp_options = get_option( 'kehittamomrp' );
	if ( ! empty( $kehittamomrp_options['post_types'] ) && in_array( $typenow, $kehittamomrp_options['post_types'] ) ) {
		kehittamomrp_find_posts_div();
	}
}

add_action( 'save_post', 'kehittamomrp_save_post' );
function kehittamomrp_save_post( $post_id )
{
	if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	if ( isset( $_POST['kehittamomrp_post_ids'], $_POST['post_ID'] ) ) {
		check_admin_referer( 'add-relastedpostsids_' . $_POST['post_ID'], '_wpnonce_yyarpp' );
        $ids = implode( ',', array_filter( wp_parse_id_list( $_POST['kehittamomrp_post_ids'] ) ) );
		update_post_meta( $_POST['post_ID'], '_yyarpp', $ids );
	}
}

add_filter( 'plugin_action_links_' . plugin_basename( KEHITTAMOMRP__FILE__ ), 'kehittamomrp_settings_action_links' );
function kehittamomrp_settings_action_links( $links )
{
	array_unshift( $links, '<a href="' . admin_url( 'admin.php?page=kehittamomrp_settings' ) . '">' . __( 'Settings' ) . '</a>' );
	return $links;
}

add_action( 'admin_menu', 'kehittamomrp_create_menu' );
function kehittamomrp_create_menu()
{
	add_options_page( KEHITTAMOMRP_VERSION, KEHITTAMOMRP_VERSION, 'manage_options', 'kehittamomrp_settings', 'kehittamomrp_settings_page' );
	register_setting( 'kehittamomrp_settings', 'kehittamomrp' );
}

register_uninstall_hook( KEHITTAMOMRP__FILE__, 'kehittamomrp_uninstaller' );
function kehittamomrp_uninstaller()
{
	global $wpdb;
	delete_option( 'kehittamomrp' );
	delete_metadata( 'post', null, '_yyarpp', null, true );
}

function kehittamomrp_settings_page()
{
	add_settings_section( 'kehittamomrp_settings_page', __( 'General', 'kehittamomrp' ), '__return_false', 'kehittamomrp_settings' );
		add_settings_field( 'kehittamomrp_field_max_posts', __( 'How many posts maximum', 'kehittamomrp' ), 'kehittamomrp_field_max_posts', 'kehittamomrp_settings', 'kehittamomrp_settings_page' );
		add_settings_field( 'kehittamomrp_field_post_types', __( 'Select post types', 'kehittamomrp' ), 'kehittamomrp_field_post_types', 'kehittamomrp_settings', 'kehittamomrp_settings_page' );
		add_settings_field( 'kehittamomrp_field_random_posts', __( 'Randomize related posts', 'kehittamomrp' ), 'kehittamomrp_field_random_posts', 'kehittamomrp_settings', 'kehittamomrp_settings_page' );
	add_settings_section( 'kehittamomrp_settings_page', __( 'Display', 'kehittamomrp' ), '__return_false', 'kehittamomrp_settings4' );
		add_settings_field( 'kehittamomrp_field_in_content', __( 'Display related posts in post content', 'kehittamomrp' ), 'kehittamomrp_field_in_content', 'kehittamomrp_settings4', 'kehittamomrp_settings_page' );
		add_settings_field( 'kehittamomrp_field_in_content_mode', __( 'Display mode', 'kehittamomrp' ), 'kehittamomrp_field_in_content_mode', 'kehittamomrp_settings4', 'kehittamomrp_settings_page' );
		add_settings_field( 'kehittamomrp_field_display_content', __( 'Display content?', 'kehittamomrp' ), 'kehittamomrp_field_display_content', 'kehittamomrp_settings4', 'kehittamomrp_settings_page' );
	add_settings_section( 'kehittamomrp_settings_page', __( 'Auto posts', 'kehittamomrp' ), 'kehittamomrp_so_ironic', 'kehittamomrp_settings2' );
		add_settings_field( 'kehittamomrp_field_auto_posts', __( 'Use auto related posts?', 'kehittamomrp' ), 'kehittamomrp_field_auto_posts', 'kehittamomrp_settings2', 'kehittamomrp_settings_page' );
	add_settings_section( 'kehittamomrp_settings_page', __( 'About' ), '__return_false', 'kehittamomrp_settings3' );
		add_settings_field( 'kehittamomrp_field_about', '', create_function( '', "include( dirname( __FILE__ ) . '/about.inc.php' );" ), 'kehittamomrp_settings3', 'kehittamomrp_settings_page' );

include( dirname( __FILE__ ) . '/setting_fields.inc.php' );
?>
	<div class="wrap" style="min-width:1000px">
	<h2><?php echo KEHITTAMOMRP_VERSION; ?> <sup>v<?php echo KEHITTAMOMRP_VERSION; ?></sup></h2>

	<form action="options.php" method="post">
		<?php settings_fields( 'kehittamomrp_settings' ); ?>
		<?php
		global $sitepress, $wpdb;
		?>
		<?php submit_button(); ?>
		<div class="tabs"><?php do_settings_sections( 'kehittamomrp_settings' ); ?></div>
		<div class="tabs"><?php do_settings_sections( 'kehittamomrp_settings4' ); ?></div>
		<div class="tabs"><?php do_settings_sections( 'kehittamomrp_settings2' ); ?></div>
		<?php submit_button(); ?>
		<div class="tabs"><?php do_settings_sections( 'kehittamomrp_settings3' ); ?></div>
	</form>
	<div>
<?php
}

add_action( 'save_post', 'kehittamomrp_purge_transient', 10, 2 );
add_action( 'deleted_post', 'kehittamomrp_purge_transient', 10, 1 );
add_action( 'clean_post_cache', 'kehittamomrp_purge_transient', 10, 2 );
add_action( 'option_permalink_structure', 'kehittamomrp_purge_transient' );
add_action( 'option_kehittamomrp', 'kehittamomrp_purge_transient' );
add_action( 'transition_post_status', 'kehittamomrp_purge_transient', 10, 3 );
function kehittamomrp_purge_transient( $a='', $b='', $c='' ) {
	global $wpdb;
	switch( current_filter() ) {
		case 'clean_post_cache':
		case 'save_post': $post_ID = $b->ID; break;
		case 'transition_post_status': $post_ID = $c->ID; break;
		case 'deleted_post': $post_ID = $a->ID; break;
		default: $post_ID = ''; break;
	}
	$wpdb->query( "DELETE from $wpdb->options WHERE option_name LIKE '%kehittamomrp_{$post_ID}%'" );
	if ( 'option_permalink_structure' == current_filter() || 'option_kehittamomrp' == current_filter() ) {
		return $a;
	}
}
