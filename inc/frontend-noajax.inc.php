<?php
defined( 'ABSPATH' ) ||	die( 'Cheatin\' uh?' );

if ( ! function_exists( 'kehittamomrp_first_image') ):
	function kehittamomrp_first_image( $post, $default ) {
		if( is_null( $post ) ) {
			return $default;
		}
		$post = ! is_a( $post, 'WP_Post' ) && (int) $post > 0 ? get_post( $post ) : $post;
		if ( is_a( $post, 'WP_Post' ) ) {
			$output = preg_match_all( '/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', do_shortcode( $post->post_content ), $matches );
			$first_img = isset( $matches[1][0] ) ? $matches[1][0] : '';
		}
		return ! empty( $first_img ) ? $first_img : $default;
	}
endif;

add_shortcode( 'manual_related_posts', 'kehittamomrp_the_content' );
add_shortcode( 'kehittamomrp', 'kehittamomrp_the_content' );

add_filter( 'the_content', 'kehittamomrp_the_content', 9 );

function get_grid_post_url($id){

	if ( has_post_thumbnail($id) ) {

		$pic = wp_get_attachment_image_src( get_post_thumbnail_id($id), 'thumbnail-sm' );
		return $pic[0];
	} else {
		return KEHITTAMOMRP_PLUGIN_URL . '/img/default-image-standard.png';
	}
}
function get_grid_post_categories($id) {
	if( get_the_category($id) ){
		foreach(( get_the_category() ) as $category) {
			if( $category->parent ){
				return $category->name; break;
			}
		}
	}
	else{
		return false;
	}
}

function kehittamomrp_the_content( $content='' ) {
	global $post;
	$kehittamomrp_options = get_option( 'kehittamomrp' );
	if ( ! $post || $kehittamomrp_options['in_content']!='on' && $content!='' || apply_filters( 'stop_kehittamomrp', false ) ) {
		return $content;
	}
	if ( ( is_home() && $kehittamomrp_options['in_homepage']=='on' && in_the_loop() ) ||
		is_singular( $kehittamomrp_options['post_types'] ) ) {
		$ids_manual = wp_parse_id_list( kehittamomrp_get_related_posts( $post->ID ) );
		$lang = isset( $_GET['lang'] ) ? $_GET['lang'] : get_locale();
		$transient_name = time().'kehittamomrp_' . $post->ID . '_' . substr( md5( serialize( $ids_manual ) . serialize( $kehittamomrp_options ) . get_permalink( $post->ID ) . $lang ), 0, 12 );
		if ( $contents = get_transient( $transient_name ) ) {
			extract( $contents );
			if ( ! empty( $list ) && is_array( $list ) && isset( $kehittamomrp_options['random_posts'] ) ) {
				shuffle( $list );
			}
			$final = $content . $head . @implode( "\n", $list ) . $foot;
			$content = apply_filters( 'kehittamomrp_posts_content', $final, $head, $content, $list, $foot );
			return $content;
		}
		$ids_auto = isset( $kehittamomrp_options['auto_posts'] ) && 'none' != $kehittamomrp_options['auto_posts'] ? kehittamomrp_get_related_posts_auto( $post ) : array();
		$ids = wp_parse_id_list( array_merge( $ids_manual, $ids_auto ) );
		if ( defined( 'ICL_LANGUAGE_CODE' ) ) {
			$head_title = isset( $kehittamomrp_options['head_titles'][ $post->post_type ][ kehittamomrp_wpml_lang_by_code( ICL_LANGUAGE_CODE ) ] ) && is_string( $kehittamomrp_options['head_titles'][ $post->post_type ][ kehittamomrp_wpml_lang_by_code( ICL_LANGUAGE_CODE ) ] )? $kehittamomrp_options['head_titles'][$post->post_type][ kehittamomrp_wpml_lang_by_code( ICL_LANGUAGE_CODE ) ] : $head_title;
		} elseif ( isset( $kehittamomrp_options['head_titles'][ $post->post_type ][ get_locale() ] ) && is_string( $kehittamomrp_options['head_titles'][ $post->post_type ][ get_locale() ] ) ){
			$head_title = $kehittamomrp_options['head_titles'][ $post->post_type ][ get_locale() ];
		} else {
			$head_title = isset( $kehittamomrp_options['head_titles'][ $post->post_type ] ) && is_string( $kehittamomrp_options['head_titles'][ $post->post_type ] ) ? $kehittamomrp_options['head_titles'][ $post->post_type ] : $head_title;
		}
		if ( ! empty( $ids ) && is_array( $ids ) && isset( $ids[0] ) && $ids[0] != 0 ) {
			$ids = wp_parse_id_list( $ids );
			$list = array();
			if ( isset( $kehittamomrp_options['random_posts'] ) ) {
				shuffle( $ids );
			}
			if ( (int) $kehittamomrp_options['max_posts'] > 0 && count( $ids ) > (int) $kehittamomrp_options['max_posts'] ) {
				$ids = array_slice( $ids, 0, (int)$kehittamomrp_options['max_posts'] );
			}
			$head = '<div class="kehittamomrp-manual-posts"><h4 class="widget widgettitle">' . $head_title . '</h4><div class="posts-grid sidebar-wide clearfix"><div class="row">';
			do_action( 'kehittamomrp_first_li' );
			$n = 0;
			foreach( $ids as $id ) {
				global $in_kehittamomrp_loop;
				$in_kehittamomrp_loop = true;
				if( in_array( $id, $ids_manual ) ) {
					$class = 'kehittamomrp_manual';
				} elseif( in_array( $id, get_option( 'sticky_posts' ) ) ) {
					$class = 'kehittamomrp_sticky';
				} elseif( in_array( $id, $ids_auto ) ) {
					$class = 'kehittamomrp_auto';
				}

				$_content = '';
				if( isset( $kehittamomrp_options['display_content'] ) ) {
					$p = get_post( $id );
					$_content = '<br />' . apply_filters( 'the_excerpt', $p->post_excerpt ) .'<p>&nbsp;</p>';
				}
				$_content = apply_filters( 'kehittamomrp_the_content', $_content, $id );
				$_content = apply_filters( 'kehittamomrp_more_content', $_content );

				if( $kehittamomrp_options['in_content_mode']=='list' ) {
          if ( class_exists( '\\Adian\\Plugins\\PostsGrid\\Widget' )) :
            $adian_posts_grid = new \Adian\Plugins\PostsGrid\Widget();
            $share_counts = $adian_posts_grid->get_share_counts( $id,  get_permalink( $id ), false );
          endif;
					$list[] = '<div class="grid-block col-xs-6 col-sm-6 col-md-6 col-lg-6">' .
								'<a href="' . esc_url( apply_filters( 'the_permalink', get_permalink( $id ) ) ) . '">' .
									'<div class="grid-block-inner">
										<img class="attachment-post-thumbnail" src="' . get_grid_post_url( $id ) . '"/>
										'. $share_counts .'
										<div class="grid-block-content">
											<div class="grid-block-text">
												<h2 class="grid-block-title">' . apply_filters( 'the_title', get_the_title( $id ) ) . '</h2>
											</div>
											<div class="grid-block-meta grid-block-byline">
											<div class="grid-block-date">' . get_the_time('d.m.Y G:i', $id ) . '</div>
											<div class="grid-block-categories">' . get_grid_post_categories( $id ) . '</div>
											</div>
										</div>
									</div>
								</a>
							</div>';
				} else {
					$no_thumb = apply_filters( 'kehittamomrp_no_thumb', admin_url( '/images/w-logo-blue.png' ), $id );
					$thumb_size = apply_filters( 'kehittamomrp_thumb_size', array( 100, 100 ) );
					if( current_theme_supports( 'post-thumbnails' ) ) {
						$thumb = has_post_thumbnail( $id ) ? get_the_post_thumbnail( $id, $thumb_size ) : '<img src="' . kehittamomrp_first_image( isset( $kehittamomrp_options['first_image'] ) && $kehittamomrp_options['first_image']=='on' ? $id : null, $no_thumb ) . '" height="' . $thumb_size[0] . '" width="' . $thumb_size[1] . '" />';
					} else {
						$thumb = '<img src="' . kehittamomrp_first_image( isset( $kehittamomrp_options['first_image'] ) && $kehittamomrp_options['first_image']=='on' ? $id : null, $no_thumb ) . '" height="' . $thumb_size[0] . '" width="' . $thumb_size[1] . '" />';
					}
					$list[] = '<li class="' . $class . '"><a href="' . esc_url( apply_filters( 'the_permalink', get_permalink( $id ) ) ) . '">' . $thumb . '<br />' . apply_filters( 'the_title', get_the_title( $id ) ) . '</a></li>';
				}
				$list = apply_filters( 'kehittamomrp_li', $list, ++$n );
			}
			do_action( 'kehittamomrp_last_li' );
			$list = apply_filters( 'kehittamomrp_list_li', $list );
			if( $kehittamomrp_options['in_content_mode']=='list' ) {
				$foot = '</div></div></div>';
			} else {
				$foot = '</ul></div><div style="clear:both;"></div>';
			}
			$final = $content . $head . implode( "\n", $list ) . $foot;
			$content = apply_filters( 'kehittamomrp_posts_content', $final, $content, $head, $list, $foot );
		} else {
			$head = '';//'<div class="kehittamomrp"><h3>' . esc_html( $head_title ) . '</h3>';
			$list = '';//<ul><li>' . __( 'No posts found.' ) . '</li></ul>';
			$foot = '';//</div>';
			$final = $content . $head . $list . $foot;
			$content = apply_filters( 'kehittamomrp_posts_content', $final, $content, $head, $list, $foot );
		}
	}
	if ( ! empty( $list ) ) {
		set_transient( $transient_name, array( 'head' => $head, 'list' => $list, 'foot' => $foot ) );
	}
	return $content;
}

add_filter( 'the_title', 'kehittamomrp_cut_words' );
function kehittamomrp_cut_words( $title ) {
	global $in_kehittamomrp_loop;
	if ( $in_kehittamomrp_loop ) {
		return wp_trim_words( $title, 10 );
	}
	return $title;
}
